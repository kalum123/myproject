<?php

namespace App\Policies;

use App\User;
use App\Tasks;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    public function destroy(User $user, Tasks $task)
    {
        return $user->id === $task->user_id;
    }
}
