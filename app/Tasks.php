<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Tasks extends Model
{

    protected $fillable = [
        'user_id','name', 'address'
    ];


    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
