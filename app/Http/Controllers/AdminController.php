<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Admin;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use Validator;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class AdminController extends Controller
{
   public function __construct() 
    {
      $this->middleware('auth:admin', ['except' => ['login', 'login_request'] ]);
      
    }
  
     public function index(){
       return Redirect::route('login_page');
     }

    public function logout(){
        Session::flush(); 
     //  Auth::logout();
      return Redirect::route('login_page');
       
    }
    public function login(){
      //  return Redirect::route('login_page');
       // return Redirect::to('/web/login');
        return view('web.login.login');
    }
    
    public function dashboard(){
    
        return view('web.admin.dashboard');
    }
    
    public function login_request(Request $req){
        
        
        $rules = [
            'email' => 'required|email|max:255|exists:admins',
            'password' => 'required|min:3',
        ];
        $data = $req->all();
        
        $validator = Validator::make($data, $rules);
        if ($validator->fails()){
       
        
          return Redirect::to('/web/login')->withInput($req->except('password'))->withErrors($validator);
        }
        else
        {
        
          $userdata = array(
    	    'email' => $req->get('email'),
    	    'password' => $req->get('password')
    	  );
    		  

            if(Auth::guard('admin')->attempt($userdata)) {
              return Redirect::intended('/web/dashboard');
            }
          else {
            
            Session::flash('error', 'Something went wrong'); 
            return Redirect::to('/web/login');
          }
          
        }

    }
    
    
}
