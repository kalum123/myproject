<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$apiPath = "App\Api\V1\Controllers\\";
	$api->post('login', $apiPath.'AuthController@login');
	$api->post('signup', $apiPath.'AuthController@signup');
	$api->post('recovery', $apiPath.'AuthController@recovery');
	$api->post('reset', $apiPath.'AuthController@reset');

	$api->get('breadcrumbs/all', $apiPath.'BreadcrumbController@all');
  $api->resource('breadcrumbs', $apiPath.'BreadcrumbController');
	$api->post('breadcrumbs/store',$apiPath.'BreadcrumbController@store');
	$api->post('breadcrumbs/show/{id}',$apiPath.'BreadcrumbController@show');
	$api->post('breadcrumbs/search',$apiPath.'BreadcrumbController@search');

	$api->get('tasks',$apiPath.'TasksController@index');
	$api->post('tasks/store',$apiPath.'TasksController@store');
	$api->delete('/task/{task}',$apiPath.'TasksController@destroy');

	$api->post('feedback',$apiPath.'FeedbackController@send_feedback');

});
