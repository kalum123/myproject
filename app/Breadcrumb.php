<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Breadcrumb extends Model
{
    protected $fillable = [
        'user_id', 'breadcrumb_id', 'name', 'lat', 'lng', 'address', 'description'
    ];


    protected $dates = ['created_at', 'updated_at'];

     public function user()
    {
        return $this->belongsTo(User::class);
    }
}
