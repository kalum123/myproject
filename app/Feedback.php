<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
  protected $fillable = [
      'name','email', 'vendor','phone','message'
  ];

    protected $dates = ['created_at', 'updated_at'];

}
