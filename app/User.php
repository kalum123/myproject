<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Tasks;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * This mutator automatically hashes the password.
     *
     * @var string
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function tasks()
    {
        return $this->hasMany(Tasks::class);
    }
	
	 public function breadcrumbs() {
        return $this->hasMany(Breadcrumb::class);
    }

}
