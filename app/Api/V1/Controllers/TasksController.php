<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Validator;
use Config;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Mail\Message;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\ValidationHttpException;
use App\Http\Requests;
use App\Tasks;
use App\Repositories\TaskRepository;


class TasksController extends Controller
{
    //protected $tasks;

	public function store(Request $request){

		$input =  $request->only('user_id',
			'name',
			'address'
		);

		 $rules = array(
			'name' => 'required',
			'address'=> 'required'

		);

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			 throw new StoreResourceFailedException('Could not update.', $validator->errors());
		}

    $task=new Tasks();

    $task->name=$request['name'];
    $task->address=$request['address'];
    $task->user_id=2;
    $task->save();

     /*$data = $request->user()->tasks()->create($input);

			return $this->response->array(['data'=>$data]);*/
    }


}
