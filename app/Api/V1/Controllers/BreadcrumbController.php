<?php
namespace App\Api\V1\Controllers;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Dingo\Api\Exception\ValidationHttpException;
use Dingo\Api\Exception\StoreResourceFailedException;
use App\Breadcrumb;
use Dingo\Api\Routing\Helpers;
use Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class BreadcrumbController extends Controller
{  
     use Helpers;
    
     public function __construct() 
    {
        $this->middleware('api.auth', ['except' =>
			[
				// 'all',
			]]);
    }

  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {	
    	  $breadcrumbs = $request->user()->breadcrumbs()->get();
          return $this->response->array(['data'=>$breadcrumbs]);
    }


    public function all(Request $request)
    {	
    	  $breadcrumbs = Breadcrumb::orderBy('id', 'desc')->get();
          // $breadcrumbs =  Breadcrumb::all()->sortByDesc("id");
          return $this->response->array(['data'=>$breadcrumbs]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {		

    	$input =  $request->only('user_id',
			'name',
			'breadcrumb_id',
			'lat',
			'lng',
			'address',
			'description'

		);
        $rules = array(
			'name' => 'required',
			'breadcrumb_id' => 'required|unique:breadcrumbs',
			'lat' => 'required',
			'lng' => 'required'
		);

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			 throw new StoreResourceFailedException('Could not create new breadcrumb.', $validator->errors());
		}
		
		//dd($input);

    	$data = $request->user()->breadcrumbs()->create($input);

  		 return $this->response->array(['data'=>$data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $breadcrumb = Breadcrumb::where('breadcrumb_id','=', $id)->first();     
          
		  return $this->response->array(['data' =>$breadcrumb]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {	
        $input =  $request->only('user_id',
			'name',
			'breadcrumb_id',
			'lat',
			'lng',
			'address',
			'description'

		);
        $rules = array(
			'name' => 'required',
			'breadcrumb_id' => 'required',
			'lat' => 'required',
			'lng' => 'required'
		);

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			 throw new StoreResourceFailedException('Could not update breadcrumb.', $validator->errors());
		}

		$input = array_add($input, "user_id", Auth::id());
		$breadcrumb = Breadcrumb::where('breadcrumb_id', $id)->update($input);
		
		 
		return $this->show($input['breadcrumb_id']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {	
    	// dd('sassasd');
        $breadcrumb = Breadcrumb::where('breadcrumb_id','=', $id)->firstOrFail();
        $breadcrumb->delete();
        return $this->response->array(['data' =>[]]);
    }
	
	public function search(Request $request){
		
		$results = Breadcrumb::where('description', 'like',$request['search'].'%')->get();
		
		return $this->response->array(['data' =>$results]);
	}
	
	
}
