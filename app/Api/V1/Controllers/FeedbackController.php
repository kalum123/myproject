<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Validator;
use Config;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Mail\Message;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\ValidationHttpException;
use App\Http\Requests;
use App\Feedback;
use App\Repositories\TaskRepository;

class FeedbackController extends Controller
{


	public function send_feedback(Request $request){

		$input =  $request->only('name',
			'email',
			'vendor',
      'phone',
      'message'
		);

		 $rules = array(
			'name' => 'required',
			'email'=> 'required|email',
      'vendor'=>'required',
      'phone' =>'required|numeric|digits:10',
      'message'=>'required'

		);

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			 throw new StoreResourceFailedException('Could not submit information.', $validator->errors());
		}

    $feedback=new Feedback();

    $feedback->name=$request['name'];
    $feedback->email=$request['email'];
    $feedback->vendor=$request['vendor'];
    $feedback->phone=$request['phone'];
    $feedback->message=$request['message'];

    $feedback->save();



    /* $data = $request->feedback()->create($input);

			return $this->response->array(['data'=>$data]);*/
    }


}
