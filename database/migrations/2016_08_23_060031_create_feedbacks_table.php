<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbacksTable extends Migration
{

    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('vendor');
            $table->string('phone');
            $table->text('message');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('feedbacks');
    }
}
