<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Breadcrumb;
class BreadcrumbsSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\Breadcrumb::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $faker = Faker::create();
        
        for ($i=0; $i < 150; $i++) {
            $breadcrumbs = $this->createBreadcrumbs($faker);
        }
        
    }
    
    public function createBreadcrumbs($faker)
    {
        $inputs = 
        [
            'user_id' => $faker->numberBetween(1,15), 
            'name' => $faker->company.'. '.$faker->companySuffix, 
            'address'=>$faker->unique()->address,
            'breadcrumb_id'=>$faker->unique()->randomNumber,
            'lat'=>$faker->randomFloat(4,6.65000000,7.920000),
            'lng'=> $faker->randomFloat(4,79.85000,81.44000),
            'description'=>$faker->paragraph($nbSentences = 3, $variableNbSentences = true)
        ];
        
        return Breadcrumb::create($inputs);
    }
}
