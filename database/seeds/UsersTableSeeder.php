<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $faker = Faker::create();
        
        for ($i=0; $i < 15; $i++) {
            $u = $this->createUser($faker);
        }
        
    }
    
    public function createUser($faker)
    {
        $inputs = 
        [
            'email' => $faker->email, 
            'password' => '1234', 
        ];
        
        return User::create($inputs);
    }

}
