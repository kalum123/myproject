<!DOCTYPE html>
<html lang="en">
<head>
    <title>Laravel Quickstart - Intermediate</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->

    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/css/jquery.datepick.css" rel="stylesheet">

    <link href="/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <nav class="navbar navbar-default">
        <!-- Navbar Contents -->
    </nav>
</div>

@yield('content')
</body>
</html>